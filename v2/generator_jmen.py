import re
from typing import Optional

from BiologicalNamesGenerator import Bio_names_generator as NameGenerator

name_generator = NameGenerator()
consonants = set("bcčdďfghjklmnňpqrřsštťvwxzž")
syllable_nuclei = set("aáeéiíloóruúů")
possessive_endings = {"m": "ův", "f": "ova", "n": "ovo"}
gender_endings = {"m": "ý", "f": "á", "n": "é"}
original_names = {line.strip() for line in open("czech_plant_names.txt")} | {
    "rys ostrovid"
}
original_words = {word for name in original_names for word in name.split()}


def possessive_adjective_stem(word: str) -> Optional[str]:
    if match := re.fullmatch(fr"(\w+)({'|'.join(possessive_endings.values())})", word):
        return match[1]
    return None


def is_possessive_adjective(word: str) -> bool:
    return possessive_adjective_stem(word) is not None


def detect_gender(noun: str) -> str:
    if noun[-1] == "a" or noun[-1] == "e":
        return "f"
    elif noun[-1] == "o":
        return "n"
    else:
        return "m"


def is_valid_name(name: str) -> bool:
    words = name.split()
    if words[0][-1] not in [*consonants, *"aeo"]:
        return False
    if is_possessive_adjective(words[0]):
        return False
    if words[0] in original_words:
        return False
    if any(len(word) <= 2 for word in words):
        return False
    if any(not any(letter in syllable_nuclei for letter in word) for word in words):
        return False
    return True


def correct_name(name: str) -> str:
    name = re.sub(r"([a-mo-z])\1", r"\1", name)
    words = name.split()
    gender = detect_gender(words[0])
    result = words[0].capitalize()
    for word in words[1:]:
        if stem := possessive_adjective_stem(word):
            result += " " + stem.capitalize() + possessive_endings[gender]
        elif word[-1] in gender_endings.values():
            result += " " + word[:-1] + gender_endings[gender]
        else:
            result += " " + word
    return result


def generate_name() -> str:
    for name in name_generator:
        if is_valid_name(name):
            return correct_name(name)
    return "Iberesterník vzpřímený"


if __name__ == "__main__":
    for _ in range(30):
        print(generate_name())
