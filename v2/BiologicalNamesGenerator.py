#!/usr/bin/env python
# coding: utf-8

import os
from itertools import islice
from random import choice, randrange
from time import time

import numpy as np
import tensorflow as tf


# Some utility functions
def names_list_to_ids(names: list, char_to_id: dict) -> list:
    return [name_to_ids(n, char_to_id) for n in names]


def name_to_ids(name: str, char_to_id: dict) -> list:
    return [char_to_id[c] for c in name]


def ids_to_name(char_inds: list, id_to_char: dict) -> str:
    print(char_inds)
    return "".join([id_to_char[i] for i in char_inds])


def ids_list_to_names(inds_names: list, id_to_char: dict) -> list:
    return [ids_to_name(ids, id_to_char) for ids in inds_names]


def lnames(sources: list, save_to_file=False) -> list:
    """ Returns a list with names used for training. """
    names = []
    for function, url in sources:
        for name in function(url):
            names.append(name)
    names = sorted(set(names))
    if save_to_file:
        with open("czech_plant_names.txt", "w") as output:
            for name in names:
                print(name, file=output)
    return names


def lwords(names: list) -> list:
    return [word for words in names for word in words.split()]


class Bio_names_generator:
    def __init__(self, model=None, names=None, temperature=0.2):
        if names is None:
            self.load_names()
        else:
            self.names = names
        self.words = self.lwords(self.names)
        self.create_vocab()
        if model is None:
            self.load_model()
        else:
            self.model = model
        self.part_gen = self.generate_part_of_word()
        self.temperature = temperature
        self.char_to_id = {c: i for i, c in enumerate(sorted(self.vocab))}
        self.id_to_char = {v: k for k, v in self.char_to_id.items()}

    # Preparatory functions:

    def create_vocab(self):
        self.vocab = set(c for w in self.names for c in w)
        # self.vocab.add('&') # Ending char.

    def build_model(self, embedding_dim=256, rnn_units=64, batch_size=1):
        """ Builds the model given some parameters of it.

            params:
                None of the params should be changed unless the model architecture is changed!
                embedding_dim: optional (256)
                rnn_unnits: optional (64)
                batch_size: optional (1) size of the batch should be 1, because the generator
                            is built to predict one name at a time. However changing it and
                            rewriting some other parts may speed up the whole process.
        """
        vocab_size = len(self.vocab)
        model = tf.keras.Sequential(
            [
                tf.keras.layers.Embedding(
                    vocab_size, embedding_dim, batch_input_shape=[batch_size, None]
                ),
                tf.keras.layers.GRU(
                    rnn_units,
                    return_sequences=True,
                    stateful=True,
                    recurrent_initializer="glorot_uniform",
                ),
                tf.keras.layers.Dense(vocab_size),
            ]
        )
        return model

    def load_model(self, path_to_model="model/last_epoch_model"):
        """ Loads the model given path"""
        model = self.build_model()
        model.load_weights(path_to_model)
        model.build(tf.TensorShape([1, None]))
        self.model = model

    def lwords(self, names: list) -> list:
        """ Creates list of all words in names. """
        return list(set([word for words in names for word in words.split()]))

    def load_names(self, path="czech_plant_names.txt"):
        """ Loads names, expects one name per line. """
        with open(path, "r") as data:
            self.names = [name for name in data]

    # The generating part:

    def generate_part_of_word(self):
        """ Generator, yields substrings of random len of words that are good by 'ok_ending'. """
        while True:
            w = choice(self.words)
            if self.ok_ending(w):
                yield w[: randrange(1, len(w))]

    def __iter__(self):
        return self

    def __next__(self) -> str:
        part = next(self.part_gen)
        t = time()
        while True:
            pred = self.generate_text(part, num_generate=54)
            name = self.get_name(pred)
            if self.ok_ending(name.split()[0]):
                break
            if time() - t > 0.3:
                # Net can get into a cycle in that case different starting part is needed
                part = next(self.part_gen)
                t = time()
        return name

    def generate_text(self, start_string: str, num_generate=60) -> str:
        """ Generates a name string of a given lenght. """

        input_eval = [self.char_to_id[s] for s in start_string]
        input_eval = tf.expand_dims(input_eval, 0)

        text_generated = []

        self.model.reset_states()
        for i in range(num_generate):
            predictions = self.model(input_eval)
            predictions = tf.squeeze(predictions, 0)

            predictions = predictions / self.temperature
            predicted_id = tf.random.categorical(predictions, num_samples=1)[
                -1, 0
            ].numpy()

            input_eval = tf.expand_dims([predicted_id], 0)

            text_generated.append(self.id_to_char[predicted_id])

        return start_string + "".join(text_generated)

    def get_name(self, NN_pred: str) -> str:
        return " ".join(NN_pred.split()[:2])

    def ok_ending(self, word):
        return not word[-1] in "ýáíéů"


if __name__ == "__main__":
    gen = Bio_names_generator()
    for name in islice(gen, 0, 20):
        print(name)
