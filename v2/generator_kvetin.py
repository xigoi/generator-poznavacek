import math
import random

from PIL import Image, ImageDraw

center_shapes = [
    "none",
    "circle0",
    "circle01",
    "circle012",
    "circle010",
    "square0",
    "square01",
    "square012",
    "square010",
]
leaf_configurations = [
    "00+0",
    "01+0",
    "00+45",
    "01+45",
    "000+0",
    "011+0",
    "000+90",
    "011+90",
    "0000+0",
    "0101+0",
    "0011+0",
    "0110+0",
]
leaf_shapes = [
    "ellipse",
    "halfellipse",
    "rectangle",
    "triangleout",
    "trianglein",
    "pieslice",
]


def random_color() -> str:
    hue = random.randrange(360)
    saturation = random.randint(80, 100)
    lightness = random.randint(40, 60)
    return f"hsl({hue},{saturation}%,{lightness}%)"


def randinterval(left: float, right: float) -> float:
    return left + random.random() * (right - left)


class Flower:
    def __init__(self, name: str):
        random.seed(name)
        self.center_shape = random.choice(center_shapes)
        self.center_colors = [random_color() for index in range(3)]
        self.center_radius = randinterval(0.1, 0.2)
        self.center_decay = randinterval(0.5, 0.9)
        self.leaf_configuration = random.choice(leaf_configurations)
        self.leaf_shapes = [random.choice(leaf_shapes) for index in range(2)]
        self.leaf_colors = [random_color() for index in range(2)]
        self.leaf_lengths = [randinterval(0.3, 0.5) for index in range(2)]
        self.leaf_widths = [randinterval(0.1, 0.3) for index in range(2)]

    def __repr__(self) -> str:
        return f"""Flower(
                {self.center_shape=}
                {self.center_colors=}
                {self.center_radius=}
                {self.center_decay=}
                {self.leaf_configuration=}
                {self.leaf_shapes=}
                {self.leaf_colors=}
                {self.leaf_lengths=}
                {self.leaf_widths=}
            )""".replace(
            " " * 12, ""
        )

    def image(self, size=640, border_ratio=0.05) -> Image:
        image = Image.new("RGBA", (size, size), "white")
        draw = ImageDraw.Draw(image)

        def coord(x):
            return size * (0.5 + x * (1 - 2 * border_ratio))

        def coords(*xs):
            return [coord(x) for x in xs]

        def leaves(angle, color, shape, length, width):
            aux_image = Image.new("RGBA", (size, size), (0, 0, 0, 0))
            aux_draw = ImageDraw.Draw(aux_image)
            half_width = width / 2
            if shape == "ellipse":
                aux_draw.ellipse(
                    coords(0, -half_width, length, half_width), fill=color
                )
                aux_draw.ellipse(
                    coords(-length, -half_width, 0, half_width), fill=color
                )
            elif shape == "halfellipse":
                aux_draw.ellipse(
                    coords(-length, -half_width, length, half_width), fill=color
                )
            elif shape == "rectangle":
                aux_draw.rectangle(
                    coords(-length, -half_width, length, half_width), fill=color
                )
            elif shape == "trianglein":
                aux_draw.polygon(
                    coords(0, 0, -length, -half_width, -length, half_width),
                    fill=color,
                )
                aux_draw.polygon(
                    coords(0, 0, length, -half_width, length, half_width), fill=color
                )
            elif shape == "triangleout":
                aux_draw.polygon(
                    coords(-length, 0, 0, -half_width, length, 0, 0, half_width),
                    fill=color,
                )
            elif shape == "pieslice":
                half_angle = math.degrees(math.asin(half_width / length))
                aux_draw.pieslice(
                    coords(-length, -length, length, length),
                    180 - half_angle,
                    180 + half_angle,
                    fill=color,
                )
                aux_draw.pieslice(
                    coords(-length, -length, length, length), -half_angle, half_angle, fill=color
                )
            image.alpha_composite(aux_image.rotate(angle))

        leaf_configuration_, leaf_rotation_ = self.leaf_configuration.split("+")
        leaf_configuration = [int(digit) for digit in leaf_configuration_]
        leaf_rotation = int(leaf_rotation_)
        for index, digit in enumerate(leaf_configuration):
            leaves(
                180 * index / len(leaf_configuration) + leaf_rotation,
                self.leaf_colors[leaf_configuration[index]],
                self.leaf_shapes[leaf_configuration[index]],
                self.leaf_lengths[leaf_configuration[index]],
                self.leaf_widths[leaf_configuration[index]],
            )

        if self.center_shape.startswith("circle"):
            center_radius = self.center_radius
            # TODO in Python 3.9: use removeprefix()
            for digit in map(int, self.center_shape[6:]):
                color = self.center_colors[digit]
                draw.ellipse(
                    coords(
                        -center_radius, -center_radius, center_radius, center_radius,
                    ),
                    fill=color,
                )
                center_radius *= self.center_decay
        elif self.center_shape.startswith("square"):
            center_radius = self.center_radius
            # TODO in Python 3.9: use removeprefix()
            for digit in map(int, self.center_shape[6:]):
                color = self.center_colors[digit]
                draw.rectangle(
                    coords(
                        -center_radius, -center_radius, center_radius, center_radius,
                    ),
                    fill=color,
                )
                center_radius *= self.center_decay
        return image


def color_test():
    image = Image.new("RGB", (640, 640), "white")
    draw = ImageDraw.Draw(image)
    for y in range(0, 640, 80):
        for x in range(0, 640, 80):
            draw.ellipse([x + 10, y + 10, x + 70, y + 70], fill=random_color())
    image.show()
