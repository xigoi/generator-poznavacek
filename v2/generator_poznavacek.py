from os import getcwd
from random import sample

from jinja2 import FileSystemLoader
from latex import build_pdf
from latex.jinja2 import make_env
from tqdm import tqdm, trange

from generator_jmen import generate_name
from generator_kvetin import Flower

columns = 4
image_scaling = 0.9 / columns
names_num = 300
test_names_num = 60
print("Generuji jména")
names = [generate_name() for index in trange(names_num)]
test_names = sample(names, test_names_num)
print("Generuji obrázky")
for name in tqdm(names):
    flower = Flower(name)
    image = flower.image()
    image.save(f"flowers/{name}.png")

print("Vytvářím dokument")
environment = make_env(loader=FileSystemLoader("."))
template = environment.get_template("poznavame_kvetiny_template.tex")
tex = template.render(
    columns=columns,
    names=names,
    test_names=test_names,
    image_scaling=image_scaling,
    enumerate=enumerate,
)
with open("poznavame_kvetiny.tex", "w") as tex_file:
    tex_file.write(tex)
pdf = build_pdf(tex, texinputs=[getcwd(), ""])
pdf.save_to("poznavame_kvetiny.pdf")
