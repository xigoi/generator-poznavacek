import re
from sys import stderr
from typing import Iterator, List
from urllib.request import urlopen


def wikipedia_table_names(url: str) -> Iterator[str]:
    def process_name(rawname: str) -> Iterator[str]:
        rawname = re.sub(r"\s+", " ", rawname).strip()
        if re.search(r"[_\d]", rawname):
            return
        match = re.match(r"(\w[\w\s.]*\w\s)(\w+/[\w/]+)", rawname)
        if match:
            for variant in re.split(r"/", match[2]):
                yield (match[1] + variant).lower()
            return
        match = re.match(r"(\w+/[\w/]+)(\s\w[\w\s.]*\w)", rawname)
        if match:
            for variant in re.split(r"/", match[1]):
                yield (variant + match[2]).lower()
            return
        for name in re.split(r"\s*/\s*", rawname):
            yield name.lower()

    page = urlopen(url).read().decode("utf-8")
    for row in re.findall(r"<tr>\n.*", page):
        if re.search(r"Český název", row):
            continue
        row = re.sub(r"<.*?>", "", row).strip()
        match = re.search(r"(\w[\w\s./]*\w)(?:\s\(([^)<]*))?", row)
        if match:
            for name in process_name(match[1]):
                yield name
            if match[2]:
                for rawname in re.findall(r"\w[\w\s./]*\w", match[2]):
                    for name in process_name(rawname):
                        yield name
        else:
            print(f"Couldn't match: '{row}'", file=stderr)


def botany_names(url: str) -> Iterator[str]:
    page = urlopen(url).read().decode("utf-8")
    for name in re.findall(r"–\s+([\w\s.]*)", page):
        if name:
            yield name.lower()


sources = [
    (
        wikipedia_table_names,
        "https://cs.wikipedia.org/wiki/Seznam_l%C3%A9%C4%8Div%C3%BDch_rostlin",
    ),
    (
        wikipedia_table_names,
        "https://cs.wikipedia.org/wiki/Seznam_nejjedovat%C4%9Bj%C5%A1%C3%ADch_rostlin",
    ),
    (botany_names, "https://botany.cz/cs/kvetena-ceske-republiky/"),
]


def get_names() -> List[str]:
    names = set()
    for function, url in sources:
        for name in function(url):
            names.add(name)
    return sorted(names)


if __name__ == "__main__":
    with open("czech_plant_names.txt", "w") as output:
        for name in get_names():
            print(name, file=output)
