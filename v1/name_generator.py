from random import choice, random, randrange
from re import sub

common_adjectives = ["obecn", "obecn", "obecn", "obecn", "domác"]

common_consonants = list("dklmnprstvz")
rarer_consonants = list("bcčďhjňřšťž") + ["ch"]
short_vowels = list("aaeěiouy")
long_vowels = list("ááééíůý") + ["ou"]

common_adjective_probability = 0.5
rarer_consonant_probability = 0.1
long_vowel_probability = 0.1
coda_probability = 0.2

genders = list("fmn")


def random_consonant():
    if random() < rarer_consonant_probability:
        return choice(rarer_consonants)
    else:
        return choice(common_consonants)


def random_vowel():
    if random() < long_vowel_probability:
        return choice(long_vowels)
    else:
        return choice(short_vowels)


substitution_rules = (
    (r"ď(?=[eěiyíý])", "d"),
    (r"ň(?=[eěiyíý])", "n"),
    (r"ť(?=[eěiyíý])", "t"),
    (r"(?<![bdmnptv])ě", "e"),
    (r"(?<=[hkr])i", "y"),
    (r"(?<=[hkr])í", "ý"),
    (r"(?<=[žščřcj])y", "i"),
    (r"(?<=[žščřcj])ý", "í"),
    (r"(?<=[žščřcj])[áé]$", "í"),
    (r"(?<=[žščřcj])[ao]$", "e"),
    (r"(.)(?=\1)", ""),
)


def generate_name():

    # To promote gender equality, all genders have the same probability
    gender = choice(genders)

    noun = ""
    for i in range(randrange((0 if gender == "m" else 1), 4)):
        noun += random_consonant() + random_vowel()
        if random() < coda_probability:
            noun += random_consonant()
    noun += random_consonant() + (
        "a"
        if gender == "f"
        else "o"
        if gender == "n"
        else random_vowel() + random_consonant()
    )
    for original, replacement in substitution_rules:
        noun = sub(original, replacement, noun)

    if random() < common_adjective_probability:
        adjective = choice(common_adjectives)
    else:
        adjective = ""
        for i in range(randrange(1, 4)):
            adjective += random_consonant() + random_vowel()
            if random() < coda_probability:
                adjective += random_consonant()
        adjective += random_consonant()
    adjective += "á" if gender == "f" else "é" if gender == "n" else "ý"
    for original, replacement in substitution_rules:
        adjective = sub(original, replacement, adjective)

    return noun.capitalize() + " " + adjective


def generate_names(count):
    names = []
    for i in range(count):
        name = generate_name()
        while name in names:
            name = generate_name()
        names.append(name)
    return sorted(names)
