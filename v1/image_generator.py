from math import pi, sqrt, tau
from random import randrange

from PIL import Image, ImageDraw

circle_offset = 1 / 2 - 1 / sqrt(tau)
ellipse_offset = (pi - 2) / tau
square_offset = (1 - sqrt(2) / 2) / 2
triangle_offset = (1 - sqrt(3) / 2) / 2

ellipse = ImageDraw.ImageDraw.ellipse
polygon = ImageDraw.ImageDraw.polygon
rectangle = ImageDraw.ImageDraw.rectangle

# The colors are extracted from an image of flowers I had to collect.
colors = (
    # Reddish
    "#be2513",
    "#ff6a57",
    # Yellowish
    "#ffc646",
    "#bca100",
    "#ffe800",
    "#6a6746",
    "#726a00",
    # Greenish
    "#70ac00",
    "#8aac56",
    "#c8e4b1",
    "#17531f",
    "#279862",
    # Blueish
    "#88a0ac",
    "#a4a5f5",
    "#554c8b",
    # Purpleish
    "#68213f",
    "#ce91a0",
    "#ae4259",
)
shapes = (
    # Circle
    (ellipse, circle_offset, circle_offset, 1 - circle_offset, 1 - circle_offset),
    # Diamond
    (polygon, 0, 0.5, 0.5, 1, 1, 0.5, 0.5, 0),
    # Ellipse, horizontal
    (ellipse, 0, ellipse_offset, 1, 1 - ellipse_offset),
    # Ellipse, vertical
    (ellipse, ellipse_offset, 0, 1 - ellipse_offset, 1),
    # Rectangle, horizontal
    (rectangle, 0, 0.25, 1, 0.75),
    # Rectangle, vertical
    (rectangle, 0.25, 0, 0.75, 1),
    # Square
    (rectangle, square_offset, square_offset, 1 - square_offset, 1 - square_offset),
    # Triangle, pointing up
    (polygon, 0, 1 - triangle_offset, 1, 1 - triangle_offset, 0.5, triangle_offset),
    # Triangle, pointing down
    (polygon, 0, triangle_offset, 1, triangle_offset, 0.5, 1 - triangle_offset),
)


def generate_image_description(num_shapes):
    return [
        [(randrange(len(colors)), randrange(len(shapes))) for x in range(num_shapes)]
        for y in range(num_shapes)
    ]


def generate_images(num, size=320, format="png", num_shapes=2, border_percent=5):

    # Generate unigue image descriptions.
    descriptions = []
    for i in range(num):
        desc = generate_image_description(num_shapes)
        while desc in descriptions:
            desc = generate_image_description(num_shapes)
        descriptions.append(desc)

    # Calculate sizes.
    tile_size = size // num_shapes
    border_size = tile_size * border_percent // 100
    usable_size = tile_size - 2 * border_size

    # Function for calculating coordinates in the image
    # based on the relative coordinates.
    def transform_coords(tile_x, tile_y, relative_x, relative_y):
        return (
            tile_x * tile_size + border_size + relative_x * usable_size,
            tile_y * tile_size + border_size + relative_y * usable_size,
        )

    # Function for drawing the individual shapes.
    def draw_shape(draw, tile_x, tile_y, color_i, shape_i):
        color = colors[color_i]
        shape = shapes[shape_i]
        drawing_func = shape[0]
        coord_pairs = zip(shape[1::2], shape[2::2])
        transformed = [transform_coords(tile_x, tile_y, *pair) for pair in coord_pairs]
        drawing_func(draw, transformed, color)

    # Draw the images.
    images = []
    for desc in descriptions:
        img = Image.new("RGB", (size, size), "#ffffff")
        draw = ImageDraw.Draw(img)
        for y in range(num_shapes):
            for x in range(num_shapes):
                color, shape = desc[y][x]
                draw_shape(draw, x, y, color, shape)
        images.append(img)

    return images
