import random
from os import mkdir

from image_generator import generate_images
from name_generator import generate_names


# Yes, the code in this function is ugly.
# What could you expect from code that generates code
# in a completely different language?
def generate_document(species_count, questions_count):

    # Verify the parameters.
    assert (
        species_count >= questions_count
    ), "There can't be more questions than species"

    # Generate needed data.
    images = generate_images(species_count)
    names = generate_names(species_count)
    chosen_species_indices = random.sample(range(species_count), questions_count)

    # Create a directory for the images if it does not already exist.
    try:
        mkdir("poznavacka_obrazky")
    except OSError:
        pass

    # Save the generated images to the directory.
    for i, image in enumerate(images):
        image.save("poznavacka_obrazky/" + str(i) + ".png")

    # The beginning of the document.
    document = r"""\documentclass{article}
\usepackage[a4paper, margin=1cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[czech]{babel}
\usepackage{graphicx}
\usepackage{ltablex}
\newcolumntype{Y}{>{\centering\arraybackslash}X}
\begin{document}
\section*{Poznáváme tvary --- Studijní materiály}
\begin{tabularx}{\textwidth}{YYYY}"""

    # Add entries for the study materials.
    # I know about string formatting, but it's not usable here
    # because I also need literal braces
    # and it would exceed the maximum line length.
    for i in range(species_count):
        document += (
            "\n"
            + r"\includegraphics[width=4cm]{poznavacka_obrazky/"
            + str(i)
            + r".png}\newline "
            + names[i]
            + (r"&" if (i + 1) % 4 else r"\\")
        )

    # The middle of the document.
    document += r"""
\end{tabularx}
\clearpage
\section*{Poznáváme tvary --- test}
\begin{tabularx}{\textwidth}{YYYY}"""

    # Add entries for the test.
    for i, j in enumerate(chosen_species_indices):
        document += (
            "\n"
            + r"\includegraphics[width=4cm]{poznavacka_obrazky/"
            + str(j)
            + r".png}\newline\rule{4cm}{0.2mm}"
            + (r"&" if (i + 1) % 4 else r"\\")
        )

    # The end of the document.
    document += r"""
\end{tabularx}
\end{document}"""

    # Save the document to a file.
    with open("poznavacka.tex", "w") as file:
        file.write(document)
